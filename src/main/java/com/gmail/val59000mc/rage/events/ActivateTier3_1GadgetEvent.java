package com.gmail.val59000mc.rage.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.rage.gadgets.Tier3_1Gadget;
import org.bukkit.event.Cancellable;

public class ActivateTier3_1GadgetEvent extends ActivateGadgetEvent implements Cancellable {


    public ActivateTier3_1GadgetEvent(HCGameAPI api, Tier3_1Gadget gadget) {
        super(api, gadget);
    }

}
