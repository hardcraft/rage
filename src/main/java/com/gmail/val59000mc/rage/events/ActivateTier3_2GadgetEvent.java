package com.gmail.val59000mc.rage.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.rage.gadgets.Tier3_2Gadget;
import org.bukkit.event.Cancellable;

public class ActivateTier3_2GadgetEvent extends ActivateGadgetEvent implements Cancellable {


    public ActivateTier3_2GadgetEvent(HCGameAPI api, Tier3_2Gadget gadget) {
        super(api, gadget);
    }

}
