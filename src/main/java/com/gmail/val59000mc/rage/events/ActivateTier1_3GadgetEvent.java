package com.gmail.val59000mc.rage.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.rage.gadgets.Tier1_3Gadget;
import org.bukkit.event.Cancellable;

public class ActivateTier1_3GadgetEvent extends ActivateGadgetEvent implements Cancellable {


    public ActivateTier1_3GadgetEvent(HCGameAPI api, Tier1_3Gadget gadget) {
        super(api, gadget);
    }

}
