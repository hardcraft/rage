package com.gmail.val59000mc.rage.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.rage.gadgets.Gadget;
import org.bukkit.event.Cancellable;

public abstract class ActivateGadgetEvent extends HCEvent implements Cancellable {

    private boolean isCancelled;
    private Gadget gadget;

    public ActivateGadgetEvent(HCGameAPI api, Gadget gadget) {
        super(api);
        this.isCancelled = false;
        this.gadget = gadget;
    }

    @Override
    public boolean isCancelled() {
        return isCancelled;
    }

    @Override
    public void setCancelled(boolean isCancelled) {
        this.isCancelled = isCancelled;
    }

    public Gadget getGadget() {
        return gadget;
    }

}
