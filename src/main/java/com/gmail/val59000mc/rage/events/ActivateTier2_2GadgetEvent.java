package com.gmail.val59000mc.rage.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.rage.gadgets.Tier2_2Gadget;
import org.bukkit.event.Cancellable;

public class ActivateTier2_2GadgetEvent extends ActivateGadgetEvent implements Cancellable {


    public ActivateTier2_2GadgetEvent(HCGameAPI api, Tier2_2Gadget gadget) {
        super(api, gadget);
    }

}
