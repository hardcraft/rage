package com.gmail.val59000mc.rage.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.rage.gadgets.Tier2_1Gadget;
import org.bukkit.event.Cancellable;

public class ActivateTier2_1GadgetEvent extends ActivateGadgetEvent implements Cancellable {


    public ActivateTier2_1GadgetEvent(HCGameAPI api, Tier2_1Gadget gadget) {
        super(api, gadget);
    }

}
