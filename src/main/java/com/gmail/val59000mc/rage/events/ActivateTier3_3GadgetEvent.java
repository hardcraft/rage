package com.gmail.val59000mc.rage.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.rage.gadgets.Tier3_3Gadget;
import org.bukkit.event.Cancellable;

public class ActivateTier3_3GadgetEvent extends ActivateGadgetEvent implements Cancellable {


    public ActivateTier3_3GadgetEvent(HCGameAPI api, Tier3_3Gadget gadget) {
        super(api, gadget);
    }

}
