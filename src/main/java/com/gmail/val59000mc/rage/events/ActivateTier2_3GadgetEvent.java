package com.gmail.val59000mc.rage.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.rage.gadgets.Tier2_3Gadget;
import org.bukkit.event.Cancellable;

public class ActivateTier2_3GadgetEvent extends ActivateGadgetEvent implements Cancellable {


    public ActivateTier2_3GadgetEvent(HCGameAPI api, Tier2_3Gadget gadget) {
        super(api, gadget);
    }

}
