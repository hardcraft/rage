package com.gmail.val59000mc.rage.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.rage.players.RagePlayer;
import org.bukkit.event.Cancellable;

public class DoubleJumpEvent extends HCEvent implements Cancellable {

    private boolean isCancelled;
    private boolean isUnlimitedJump;
    private RagePlayer ragePlayer;

    public DoubleJumpEvent(HCGameAPI api, RagePlayer ragePlayer) {
        super(api);
        this.isUnlimitedJump = false;
        this.isCancelled = false;
        this.ragePlayer = ragePlayer;
    }

    @Override
    public boolean isCancelled() {
        return isCancelled;
    }

    @Override
    public void setCancelled(boolean isCancelled) {
        this.isCancelled = isCancelled;
    }

    public boolean isUnlimitedJump() {
        return isUnlimitedJump;
    }

    public void setUnlimitedJump(boolean isUnlimitedJump) {
        this.isUnlimitedJump = isUnlimitedJump;
    }

    public RagePlayer getRagePlayer() {
        return ragePlayer;
    }


}
