package com.gmail.val59000mc.rage.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.rage.gadgets.Tier1_1Gadget;
import org.bukkit.event.Cancellable;

public class ActivateTier1_1GadgetEvent extends ActivateGadgetEvent implements Cancellable {


    public ActivateTier1_1GadgetEvent(HCGameAPI api, Tier1_1Gadget gadget) {
        super(api, gadget);
    }

}
