package com.gmail.val59000mc.rage.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.rage.gadgets.Tier1_2Gadget;
import org.bukkit.event.Cancellable;

public class ActivateTier1_2GadgetEvent extends ActivateGadgetEvent implements Cancellable {


    public ActivateTier1_2GadgetEvent(HCGameAPI api, Tier1_2Gadget gadget) {
        super(api, gadget);
    }

}
