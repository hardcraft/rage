package com.gmail.val59000mc.rage.commands;

import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.rage.items.RageItems;
import com.gmail.val59000mc.rage.players.RagePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class RageTestCommand extends HCCommand {


    public RageTestCommand(JavaPlugin plugin) {
        super(plugin, "test");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {

            Player player = (Player) sender;

            if (args.length == 0) {
                player.sendMessage("§cMissing argument !");
            } else {
                switch (args[0]) {
                    case "tier11":
                        giveTier1_1(player);
                        break;
                    case "tier12":
                        giveTier1_2(player);
                        break;
                    case "tier13":
                        giveTier1_3(player);
                        break;
                    case "tier21":
                        giveTier2_1(player);
                        break;
                    case "tier22":
                        giveTier2_2(player);
                        break;
                    case "tier23":
                        giveTier2_3(player);
                        break;
                    case "tier31":
                        giveTier3_1(player);
                        break;
                    case "tier32":
                        giveTier3_2(player);
                        break;
                    case "tier33":
                        giveTier3_3(player);
                        break;
                    default:
                        player.sendMessage("§cUnknown command !");
                        break;
                }
            }
        }
        return true;
    }

    private RagePlayer getRagePlayer(Player player) {
        return (RagePlayer) getPmApi().getHCPlayer(player);
    }

    private void giveTier1_1(Player player) {
        player.getInventory().addItem(RageItems.getTier1_1Gadget(getRagePlayer(player)));
    }

    private void giveTier1_2(Player player) {
        player.getInventory().addItem(RageItems.getTier1_2Gadget(getRagePlayer(player)));
    }

    private void giveTier1_3(Player player) {
        player.getInventory().addItem(RageItems.getTier1_3Gadget(getRagePlayer(player)));
    }

    private void giveTier2_1(Player player) {
        player.getInventory().addItem(RageItems.getTier2_1Gadget(getRagePlayer(player)));
    }

    private void giveTier2_2(Player player) {
        player.getInventory().addItem(RageItems.getTier2_2Gadget(getRagePlayer(player)));
    }

    private void giveTier2_3(Player player) {
        player.getInventory().addItem(RageItems.getTier2_3Gadget(getRagePlayer(player)));
    }

    private void giveTier3_1(Player player) {
        player.getInventory().addItem(RageItems.getTier3_1Gadget(getRagePlayer(player)));
    }

    private void giveTier3_2(Player player) {
        player.getInventory().addItem(RageItems.getTier3_2Gadget(getRagePlayer(player)));
    }

    private void giveTier3_3(Player player) {
        player.getInventory().addItem(RageItems.getTier3_3Gadget(getRagePlayer(player)));
    }


}
