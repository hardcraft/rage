package com.gmail.val59000mc.rage.gadgets;

import com.gmail.val59000mc.rage.players.RagePlayer;
import net.minecraft.server.v1_14_R1.EntityLiving;
import net.minecraft.server.v1_14_R1.EntityTNTPrimed;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftTNTPrimed;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;

import java.lang.reflect.Field;

public class Tier2_1Gadget extends Gadget {

    public Tier2_1Gadget(RagePlayer ragePlayer) {
        super(ragePlayer, "tier-2-1");
    }

    @Override
    protected void onGadgetActivated() {
        activatedGadget();
        Player player = ragePlayer.getPlayer();
        getSoundApi().play(Sound.ITEM_FIRECHARGE_USE, player.getLocation(), 0.5f, 2f);
        spawnTnt(player, ragePlayer.getTier2_1Value());
        getApi().unregisterListener(Tier2_1Gadget.this);
    }

    private void spawnTnt(Player player, float yield) {


        // Spawn a tnt
        World world = player.getWorld();
        TNTPrimed tnt = (TNTPrimed) world.spawnEntity(player.getLocation(), EntityType.PRIMED_TNT);
        tnt.setFuseTicks(0);
        tnt.setIsIncendiary(false);
        tnt.setYield(yield / 2);

        // Change via NMS the source of the TNT by the player
        EntityLiving nmsEntityLiving = (((CraftLivingEntity) player).getHandle());
        EntityTNTPrimed nmsTNT = (((CraftTNTPrimed) tnt).getHandle());
        nmsTNT.setInvisible(true);
        try {
            Field source = EntityTNTPrimed.class.getDeclaredField("source");
            source.setAccessible(true);
            source.set(nmsTNT, nmsEntityLiving);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // draw sphere of particles
        int sqrtParticles = Math.round(yield) * 5;
        float theta = 0;
        float phi = 0;
        float thetaStep = (float) Math.PI / (float) sqrtParticles;
        float phiStep = 2 * (float) Math.PI / (float) sqrtParticles;
        float radius = yield;

        for (int i = 0; i < sqrtParticles; i++) {
            theta += thetaStep;
            phi = 0;
            for (int j = 0; j < sqrtParticles; j++) {
                phi += phiStep;
                float x = (radius * (float) Math.sin(theta) * (float) Math.cos(phi));
                float y = (radius * (float) Math.sin(theta) * (float) Math.sin(phi));
                float z = (radius * (float) Math.cos(theta));
                world.spawnParticle(Particle.FLAME, tnt.getLocation().add(x, y, z), 1, 0, 0, 0, 1, null, true);
            }
        }

    }

}
