package com.gmail.val59000mc.rage.gadgets.checker;

import com.gmail.val59000mc.spigotutils.ItemCompareOption;
import org.bukkit.Material;

public abstract class AbstractGadgetChecker implements IGadgetChecker {

    static ItemCompareOption compareGadget = new ItemCompareOption.Builder()
        .withDisplayName(true)
        .withEnchantments(true)
        .withMaterial(true)
        .withLore(true)
        .build();

    private Material material;

    public AbstractGadgetChecker(Material material) {
        this.material = material;
    }

    public Material getMaterial() {
        return material;
    }


}
