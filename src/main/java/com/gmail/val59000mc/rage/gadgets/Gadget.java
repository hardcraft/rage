package com.gmail.val59000mc.rage.gadgets;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCBeforeEndEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDeathEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.rage.events.ActivateGadgetEvent;
import com.gmail.val59000mc.rage.players.RagePlayer;
import com.gmail.val59000mc.spigotutils.Time;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

public abstract class Gadget extends HCListener {

    protected RagePlayer ragePlayer;
    protected String gadgetName;

    public Gadget(RagePlayer ragePlayer, String gadgetName) {
        this.ragePlayer = ragePlayer;
        this.gadgetName = gadgetName;
    }


    public RagePlayer getRagePlayer() {
        return ragePlayer;
    }

    public final void activate(HCGameAPI hcGameAPI) {
        hcGameAPI.registerListener(this);
        onGadgetActivated();
    }

    /**
     * Callback after activation to play sound or display messages
     */
    protected abstract void onGadgetActivated();

    protected void activatedGadget() {
        getStringsApi().get("rage.gadgets." + gadgetName + ".activated-all")
            .replace("%player%", ragePlayer.getColoredName())
            .sendChatP();

        getStringsApi().get("rage.gadgets." + gadgetName + ".activated-self")
            .sendActionBar(ragePlayer);
    }

    protected void endedGadget() {
        getStringsApi().get("rage.gadgets." + gadgetName + ".ended")
            .sendActionBar(ragePlayer);

        getSoundApi().play(ragePlayer, Sound.BLOCK_CHEST_CLOSE, 0.5f, 2f);
    }

    protected void remainingTimeGadget(int time) {
        getStringsApi().get("rage.gadgets." + gadgetName + ".remaining-time")
            .replace("%time%", Time.getFormattedTime(time))
            .sendActionBar(ragePlayer);
    }

    protected void denyActivateGadget() {
        getStringsApi().get("rage.gadgets." + gadgetName + ".in-use")
            .sendChat(ragePlayer);
        getSoundApi().play(ragePlayer, Sound.ENTITY_VILLAGER_NO, 0.5f, 2);
    }

    @EventHandler
    public void onDeath(HCPlayerDeathEvent e) {
        if (e.getHcPlayer().equals(ragePlayer)) {
            getStringsApi().get("rage.gadgets.all.death").sendChat(ragePlayer);
            getApi().unregisterListener(this);
        }
    }

    @EventHandler
    public void beforeEnd(HCBeforeEndEvent e) {
        getApi().unregisterListener(this);
    }


    @EventHandler(priority = EventPriority.NORMAL)
    public void onActivateSameGadget(ActivateGadgetEvent e) {
        if (e.getGadget().getRagePlayer().equals(ragePlayer)) {
            denyActivateGadget();
            e.setCancelled(true);
        }
    }

}
