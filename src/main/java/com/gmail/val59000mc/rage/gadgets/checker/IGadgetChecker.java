package com.gmail.val59000mc.rage.gadgets.checker;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.rage.events.ActivateGadgetEvent;
import com.gmail.val59000mc.rage.players.RagePlayer;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public interface IGadgetChecker {

    Material getMaterial();

    boolean isGadgetItem(ItemStack item);

    ActivateGadgetEvent getActivationEvent(HCGameAPI api, RagePlayer ragePlayer);

    void addSavedGagets(RagePlayer ragePlayer, ItemStack[] inv, List<ItemStack> savedGadgets);

}
