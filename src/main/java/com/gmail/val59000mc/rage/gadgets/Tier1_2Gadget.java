package com.gmail.val59000mc.rage.gadgets;

import com.gmail.val59000mc.rage.players.RagePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class Tier1_2Gadget extends Gadget {

    public Tier1_2Gadget(RagePlayer ragePlayer) {
        super(ragePlayer, "tier-1-2");
    }

    @Override
    protected void onGadgetActivated() {
        activatedGadget();

        Player player = ragePlayer.getPlayer();
        getSoundApi().play(Sound.ENTITY_ENDER_DRAGON_FLAP, player.getLocation(), 0.5f, 2f);
        propelPlayer(player, ragePlayer.getTier1_2Value());
        getApi().unregisterListener(this);
    }

    private void propelPlayer(Player player, int value) {
        // if on ground , push a little upward first
        if (player.getLocation().add(0, -1, 0).getBlock().getType().isSolid()) {
            player.setVelocity(new Vector(0, 0.5, 0));
            getApi().sync(() -> {
                player.setVelocity(player.getLocation().getDirection().normalize().multiply(value));
            }, 3);
        } else {
            player.setVelocity(player.getLocation().getDirection().normalize().multiply(value));
        }
    }


}
