package com.gmail.val59000mc.rage.gadgets;

import com.gmail.val59000mc.hcgameslib.events.HCBeforeEndEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDeathEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerKilledByPlayerEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.rage.items.RageItems;
import com.gmail.val59000mc.rage.players.RagePlayer;
import com.gmail.val59000mc.spigotutils.Colors;
import org.bukkit.*;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.util.Vector;

import java.util.List;

public class Tier2_3Gadget extends Gadget {

    public Tier2_3Gadget(RagePlayer ragePlayer) {
        super(ragePlayer, "tier-2-3");
    }

    @Override
    protected void onGadgetActivated() {
        activatedGadget();

        Player player = ragePlayer.getPlayer();
        getSoundApi().play(Sound.ENTITY_CHICKEN_HURT, player.getLocation(), 0.5f, 1.5f);

        getApi().buildTask("hardcoins punch for " + ragePlayer.getName(), (task) -> {
            remainingTimeGadget(task.getScheduler().getIterations());
        }).withLastCallback((task) -> {
            endedGadget();
            getApi().unregisterListener(Tier2_3Gadget.this);
        })
            .addListener(new HCTaskListener() {
                @EventHandler
                public void onDeath(HCPlayerDeathEvent e) {
                    if (e.getHcPlayer().equals(ragePlayer))
                        getScheduler().stop();
                }

                @EventHandler
                public void beforeEnd(HCBeforeEndEvent e) {
                    getScheduler().stop();
                }

                @EventHandler
                public void onKill(HCPlayerKilledByPlayerEvent e) {
                    if (e.getHcKiller().equals(ragePlayer)) {
                        spawnHardcoins(e.getHCKilled());
                    }
                }
            })
            .withInterval(20)
            .withIterations(ragePlayer.getTier2_3Value())
            .build()
            .start();
    }

    private void spawnHardcoins(HCPlayer victim) {
        if (victim.isOnline()) {

            Location location = victim.getPlayer().getLocation();

            List<ItemStack> hardcoins = RageItems.getHardcoins(8);

            World world = location.getWorld();

            // 8 loops
            for (int i = 0; i < 625; i += 79) {
                Item item = world.dropItem(location, hardcoins.remove(hardcoins.size() - 1));
                item.setVelocity(new Vector(Math.cos((double) i / 100d), 1, Math.sin((double) i / 100d)).multiply(0.25));
            }

            spawnFirework(location, Colors.toColor(victim.getColor()));
        }
    }

    private void spawnFirework(Location location, Color color) {
        Firework fw = (Firework) location.getWorld().spawnEntity(location, EntityType.FIREWORK);
        FireworkMeta fwm = fw.getFireworkMeta();

        Color secondaryColor = Color.fromRGB(255, 255, 255);

        //Create our effect with this
        FireworkEffect effect = FireworkEffect.builder()
            .with(Type.BALL)
            .flicker(true)
            .withColor(color, secondaryColor)
            .withFade(color, secondaryColor)
            .build();

        //Then apply the effect to the meta
        fwm.addEffect(effect);

        //Generate some random power and set it
        fwm.setPower(1);

        //Then apply this to our rocket
        fw.setFireworkMeta(fwm);

        getApi().sync(() -> fw.detonate(), 5);
    }


}
