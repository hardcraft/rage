package com.gmail.val59000mc.rage.gadgets;

import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.rage.players.RagePlayer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class Tier1_1Gadget extends Gadget {

    public Tier1_1Gadget(RagePlayer ragePlayer) {
        super(ragePlayer, "tier-1-1");
    }

    @Override
    protected void onGadgetActivated() {
        activatedGadget();

        Player player = ragePlayer.getPlayer();
        getSoundApi().play(Sound.ENTITY_ARROW_SHOOT, player.getLocation(), 0.5f, 2f);
        player.getInventory().addItem(new ItemStack(Material.ARROW));
    }

    @EventHandler
    public void onShootArrow(EntityShootBowEvent e) {
        Entity entity = e.getEntity();
        if (entity instanceof Player && ragePlayer.getUuid().equals(entity.getUniqueId())) {
            if (ragePlayer.isPlaying()) {
                Arrow originalArrow = (Arrow) e.getProjectile();
                originalArrow.setFireTicks(10000);
                Player player = (Player) entity;

                Vector originalVelocity = originalArrow.getVelocity();
                Vector perpendicular = new Vector(originalVelocity.getZ(), 0, -originalVelocity.getX()).normalize();

                int halfArrows = (ragePlayer.getTier1_1Value() - 1) / 2;

                for (int i = -halfArrows; i <= halfArrows; i++) {
                    if (i != 0) {
                        Vector velocity = originalVelocity.clone().add(perpendicular.clone().multiply(i * (1.125 - e.getForce())));
                        Arrow arrow = player.launchProjectile(Arrow.class, velocity);
                        arrow.setDamage(10000);
                        arrow.setFireTicks(10000);
                        traceArrowWithParticles(arrow);
                    }
                }

                traceArrowWithParticles(originalArrow);
            }

            getApi().unregisterListener(this);
        }

    }

    private void traceArrowWithParticles(Arrow arrow) {

        getApi().buildTask("trace arrow with particles", task -> {
            if (arrow.isValid()) {
                Location location = arrow.getLocation();
                location.getWorld().spawnParticle(Particle.FLAME, location, 1, 0, 0, 0, 15, null, true);
                getSoundApi().play(Sound.UI_BUTTON_CLICK, location, 0.2f, 2f);
            } else {
                task.stop();
            }
        })
            .addListener(new HCTaskListener() {
                @EventHandler
                public void onProjectileHit(ProjectileHitEvent e) {
                    if (e.getEntity().equals(arrow)) {
                        getScheduler().stop();
                    }
                }
            })
            .withInterval(1)
            .build()
            .start();

    }

}
