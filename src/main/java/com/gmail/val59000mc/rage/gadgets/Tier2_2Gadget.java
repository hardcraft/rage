package com.gmail.val59000mc.rage.gadgets;

import com.gmail.val59000mc.hcgameslib.events.HCBeforeEndEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDeathEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.rage.players.RagePlayer;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Tier2_2Gadget extends Gadget {

    private static final int MAX_FLAME_DISTANCE = 6;

    private int onGoingFlameThrowerParticles;

    public Tier2_2Gadget(RagePlayer ragePlayer) {
        super(ragePlayer, "tier-2-2");
        this.onGoingFlameThrowerParticles = 0;
    }

    @Override
    protected void onGadgetActivated() {
        activatedGadget();

        Player player = ragePlayer.getPlayer();
        getSoundApi().play(Sound.ENTITY_GHAST_SHOOT, player.getLocation(), 0.5f, 0.5f);

        int flameDuration = ragePlayer.getTier2_2Value();

        getApi().buildTask("flame thrower for " + ragePlayer.getName(), (task) -> {
            if (task.getScheduler().getIterations() % 10 == 0) {
                remainingTimeGadget((task.getScheduler().getIterations() / 10) + 1);
                getSoundApi().play(Sound.BLOCK_FIRE_AMBIENT, player.getLocation(), 0.5f, 0.5f);
            }
            throwFlames(player);
        })
            .addListener(new HCTaskListener() {
                @EventHandler
                public void onDeath(HCPlayerDeathEvent e) {
                    if (e.getHcPlayer().equals(ragePlayer))
                        getScheduler().stop();
                }

                @EventHandler
                public void beforeEnd(HCBeforeEndEvent e) {
                    getScheduler().stop();
                }
            })
            .withInterval(2)
            .withIterations(flameDuration * 10)
            .build()
            .start();
    }

    private void throwFlames(Player player) {

        // calculate locations async and throw flames
        getApi().async(() -> calculateFlameLocations(player.getLocation().add(0, 1, 0)), (locations) -> {

            onGoingFlameThrowerParticles++;

            getApi().buildTask("flame thrower particles for " + ragePlayer.getName(), (task) -> {
                if (locations.isEmpty()) {
                    onGoingFlameThrowerParticles--;
                    checkFlameThrowerAllDone();
                    task.stop();
                } else {
                    Location flameLocation = locations.remove(locations.size() - 1);
                    flameLocation.getWorld().spawnParticle(Particle.FLAME, flameLocation, 1, 0.1d, 0.1d, 0.1d, 1, null, true);
                    damageNearbyPlayers(flameLocation);
                }
            })
                .withInterval(1)
                .build()
                .start();
        });

    }

    private void damageNearbyPlayers(Location flameLocation) {
        Collection<Entity> entities = flameLocation.getWorld().getNearbyEntities(flameLocation, 0.5, 0.5, 0.5);
        if (ragePlayer.isPlaying()) {
            Player flameThrower = ragePlayer.getPlayer();
            for (Entity entity : entities) {
                if (entity instanceof Player) {
                    Player player = (Player) entity;
                    HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
                    if (hcPlayer != null
                        && hcPlayer.isPlaying()
                        && !hcPlayer.isInTeamWith(ragePlayer)) {
                        player.damage(15, flameThrower);
                        entity.setFireTicks(60);
                    }
                }
            }
        }

    }

    private List<Location> calculateFlameLocations(Location playerLoc) {
        Vector vector = playerLoc.clone().getDirection().normalize();

        List<Location> flameLocations = new ArrayList<>();
        for (int i = 3 * MAX_FLAME_DISTANCE; i >= 0; i--) {

            flameLocations.add(
                playerLoc.clone().add(vector.clone().multiply(((double) i / 3d)))
            );
        }

        return flameLocations;
    }


    private void checkFlameThrowerAllDone() {
        if (onGoingFlameThrowerParticles == 0) {
            endedGadget();
            getApi().unregisterListener(this);
        }
    }


}
