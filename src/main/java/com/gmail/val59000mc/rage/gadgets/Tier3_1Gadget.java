package com.gmail.val59000mc.rage.gadgets;

import com.gmail.val59000mc.hcgameslib.events.HCBeforeEndEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDeathEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.rage.events.DoubleJumpEvent;
import com.gmail.val59000mc.rage.players.RagePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

public class Tier3_1Gadget extends Gadget {

    public Tier3_1Gadget(RagePlayer ragePlayer) {
        super(ragePlayer, "tier-3-1");
    }

    @Override
    protected void onGadgetActivated() {
        activatedGadget();
        Player player = ragePlayer.getPlayer();
        getSoundApi().play(Sound.ENTITY_ENDER_DRAGON_FLAP, player.getLocation(), 0.5f, 1.5f);
        startUnlimitedJumpTask();
    }

    private void startUnlimitedJumpTask() {
        getApi().buildTask("unlimited jumps for " + ragePlayer.getName(), (task) -> {
            remainingTimeGadget(task.getScheduler().getIterations() + 1);
        }).withLastCallback((task) -> {
            endedGadget();
            getApi().unregisterListener(Tier3_1Gadget.this);
        })
            .addListener(new HCTaskListener() {
                @EventHandler
                public void onDeath(HCPlayerDeathEvent e) {
                    if (e.getHcPlayer().equals(ragePlayer))
                        getScheduler().stop();
                }

                @EventHandler
                public void beforeEnd(HCBeforeEndEvent e) {
                    getScheduler().stop();
                }
            })
            .withInterval(20)
            .withIterations(ragePlayer.getTier3_1Value())
            .build()
            .start();
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onDoubleJump(DoubleJumpEvent e) {
        if (e.getRagePlayer().equals(ragePlayer)) {
            e.setUnlimitedJump(true);
        }
    }

}
