package com.gmail.val59000mc.rage.gadgets;

import com.gmail.val59000mc.hcgameslib.events.HCBeforeEndEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDeathEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.rage.items.RageItems;
import com.gmail.val59000mc.rage.players.RagePlayer;
import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Inventories;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.potion.PotionEffectType;

public class Tier3_2Gadget extends Gadget {

    private boolean hasWhiteStuff;

    public Tier3_2Gadget(RagePlayer ragePlayer) {
        super(ragePlayer, "tier-3-2");
        this.hasWhiteStuff = false;
    }

    @Override
    protected void onGadgetActivated() {
        activatedGadget();

        Player player = ragePlayer.getPlayer();
        Effects.add(player, PotionEffectType.DAMAGE_RESISTANCE, 1 + ragePlayer.getTier3_2Value() * 20, 255, true);
        getSoundApi().play(Sound.BLOCK_ANVIL_LAND, player.getLocation(), 0.5f, 0.5f);

        getApi().buildTask("invincibility for " + ragePlayer.getName(), (task) -> {
            if (task.getScheduler().getIterations() % 4 == 0) {
                remainingTimeGadget(task.getScheduler().getIterations() / 4);
            }


            if (hasWhiteStuff) {
                Inventories.setArmor(player, RageItems.getArmor(ragePlayer));
                hasWhiteStuff = false;
            } else {
                Inventories.setArmor(player, RageItems.getWhiteArmor());
                hasWhiteStuff = true;
            }

            getSoundApi().play(Sound.BLOCK_NOTE_BLOCK_PLING, player.getLocation(), 0.2f, 2f);


            // switch stuff
        }).withLastCallback((task) -> {
            endedGadget();
            Inventories.setArmor(player, RageItems.getArmor(ragePlayer));
            getApi().unregisterListener(Tier3_2Gadget.this);
        })
            .addListener(new HCTaskListener() {
                @EventHandler
                public void onDeath(HCPlayerDeathEvent e) {
                    if (e.getHcPlayer().equals(ragePlayer))
                        getScheduler().stop();
                }

                @EventHandler
                public void beforeEnd(HCBeforeEndEvent e) {
                    getScheduler().stop();
                }
            })
            .withInterval(5)
            .withIterations(ragePlayer.getTier3_2Value() * 4)
            .build()
            .start();
    }


}
