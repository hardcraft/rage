package com.gmail.val59000mc.rage.gadgets.checker;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.rage.events.ActivateGadgetEvent;
import com.gmail.val59000mc.rage.events.ActivateTier2_2GadgetEvent;
import com.gmail.val59000mc.rage.gadgets.Tier2_2Gadget;
import com.gmail.val59000mc.rage.items.RageItems;
import com.gmail.val59000mc.rage.players.RagePlayer;
import com.gmail.val59000mc.spigotutils.Inventories;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class Gadget2_2Checker extends AbstractGadgetChecker {

    public static Material itemMaterial = Material.GLOWSTONE_DUST;

    public Gadget2_2Checker() {
        super(itemMaterial);
    }

    @Override
    public boolean isGadgetItem(ItemStack item) {
        return RageItems.isTier2_2Gadget(item);
    }

    @Override
    public ActivateGadgetEvent getActivationEvent(HCGameAPI api, RagePlayer ragePlayer) {
        return new ActivateTier2_2GadgetEvent(api, new Tier2_2Gadget(ragePlayer));
    }

    @Override
    public void addSavedGagets(RagePlayer ragePlayer, ItemStack[] inv, List<ItemStack> savedGadgets) {
        int count = Inventories.countOccurrences(inv, RageItems.getTier2_2Gadget(ragePlayer), compareGadget);
        if (count > 0) savedGadgets.add(RageItems.getTier2_2Gadget(ragePlayer, count));
    }

}
