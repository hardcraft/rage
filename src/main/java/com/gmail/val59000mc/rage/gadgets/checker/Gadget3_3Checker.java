package com.gmail.val59000mc.rage.gadgets.checker;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.rage.events.ActivateGadgetEvent;
import com.gmail.val59000mc.rage.events.ActivateTier3_3GadgetEvent;
import com.gmail.val59000mc.rage.gadgets.Tier3_3Gadget;
import com.gmail.val59000mc.rage.items.RageItems;
import com.gmail.val59000mc.rage.players.RagePlayer;
import com.gmail.val59000mc.spigotutils.Inventories;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class Gadget3_3Checker extends AbstractGadgetChecker {

    public static Material itemMaterial = Material.BRICK;

    public Gadget3_3Checker() {
        super(itemMaterial);
    }

    @Override
    public boolean isGadgetItem(ItemStack item) {
        return RageItems.isTier3_3Gadget(item);
    }

    @Override
    public ActivateGadgetEvent getActivationEvent(HCGameAPI api, RagePlayer ragePlayer) {
        return new ActivateTier3_3GadgetEvent(api, new Tier3_3Gadget(ragePlayer));
    }

    @Override
    public void addSavedGagets(RagePlayer ragePlayer, ItemStack[] inv, List<ItemStack> savedGadgets) {
        int count = Inventories.countOccurrences(inv, RageItems.getTier3_3Gadget(ragePlayer), compareGadget);
        if (count > 0) savedGadgets.add(RageItems.getTier3_3Gadget(ragePlayer, count));
    }

}
