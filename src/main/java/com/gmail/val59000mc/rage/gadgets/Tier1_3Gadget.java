package com.gmail.val59000mc.rage.gadgets;

import com.gmail.val59000mc.rage.players.RagePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class Tier1_3Gadget extends Gadget {

    public Tier1_3Gadget(RagePlayer ragePlayer) {
        super(ragePlayer, "tier-1-3");
    }

    @Override
    protected void onGadgetActivated() {
        activatedGadget();

        Player player = ragePlayer.getPlayer();
        getSoundApi().play(Sound.ENTITY_VILLAGER_NO, player.getLocation(), 0.5f, 2f);
        getApi().unregisterListener(this);
    }


}
