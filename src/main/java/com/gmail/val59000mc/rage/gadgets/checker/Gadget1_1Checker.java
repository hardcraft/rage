package com.gmail.val59000mc.rage.gadgets.checker;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.rage.events.ActivateGadgetEvent;
import com.gmail.val59000mc.rage.events.ActivateTier1_1GadgetEvent;
import com.gmail.val59000mc.rage.gadgets.Tier1_1Gadget;
import com.gmail.val59000mc.rage.items.RageItems;
import com.gmail.val59000mc.rage.players.RagePlayer;
import com.gmail.val59000mc.spigotutils.Inventories;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class Gadget1_1Checker extends AbstractGadgetChecker {

    public static Material itemMaterial = Material.STRING;

    public Gadget1_1Checker() {
        super(itemMaterial);
    }

    @Override
    public boolean isGadgetItem(ItemStack item) {
        return RageItems.isTier1_1Gadget(item);
    }

    @Override
    public ActivateGadgetEvent getActivationEvent(HCGameAPI api, RagePlayer ragePlayer) {
        return new ActivateTier1_1GadgetEvent(api, new Tier1_1Gadget(ragePlayer));
    }

    @Override
    public void addSavedGagets(RagePlayer ragePlayer, ItemStack[] inv, List<ItemStack> savedGadgets) {
        int count = Inventories.countOccurrences(inv, RageItems.getTier1_1Gadget(ragePlayer), compareGadget);
        if (count > 0) savedGadgets.add(RageItems.getTier1_1Gadget(ragePlayer, count));
    }

}
