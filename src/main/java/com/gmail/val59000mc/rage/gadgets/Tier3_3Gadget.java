package com.gmail.val59000mc.rage.gadgets;

import com.gmail.val59000mc.hcgameslib.events.HCBeforeEndEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDeathEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerRewardBonusMultiplierEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.rage.players.RagePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

public class Tier3_3Gadget extends Gadget {

    public Tier3_3Gadget(RagePlayer ragePlayer) {
        super(ragePlayer, "tier-3-3");
    }

    @Override
    protected void onGadgetActivated() {
        activatedGadget();

        Player player = ragePlayer.getPlayer();
        getSoundApi().play(Sound.ENTITY_PLAYER_LEVELUP, player.getLocation(), 0.5f, 0.5f);

        getApi().buildTask("double coins for " + ragePlayer.getName(), (task) -> {
            remainingTimeGadget(task.getScheduler().getIterations() + 1);
        }).withLastCallback((task) -> {
            endedGadget();
            getApi().unregisterListener(Tier3_3Gadget.this);
        })
            .addListener(new HCTaskListener() {
                @EventHandler
                public void onDeath(HCPlayerDeathEvent e) {
                    if (e.getHcPlayer().equals(ragePlayer))
                        getScheduler().stop();
                }

                @EventHandler
                public void onRewardMoney(HCPlayerRewardBonusMultiplierEvent e) {
                    if (e.getHcPlayer().equals(ragePlayer))
                        e.addMultiplier(2);
                }

                @EventHandler
                public void beforeEnd(HCBeforeEndEvent e) {
                    getScheduler().stop();
                }
            })
            .withInterval(20)
            .withIterations(ragePlayer.getTier3_3Value())
            .build()
            .start();
    }


}
