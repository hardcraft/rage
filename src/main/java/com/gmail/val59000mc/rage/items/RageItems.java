package com.gmail.val59000mc.rage.items;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.rage.gadgets.checker.*;
import com.gmail.val59000mc.rage.players.RagePlayer;
import com.gmail.val59000mc.spigotutils.Colors;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.gmail.val59000mc.spigotutils.items.ItemBuilder;
import com.gmail.val59000mc.spigotutils.items.LeatherArmorMetaBuilder;
import com.google.common.collect.Lists;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class RageItems {

    private static HCGameAPI api;

    public static void setup(HCGameAPI api) {
        RageItems.api = api;
    }

    public static List<ItemStack> getWhiteArmor() {
        Color color = Color.WHITE;
        return Lists.newArrayList(
            new ItemBuilder(Material.LEATHER_HELMET).buildMeta(LeatherArmorMetaBuilder.class).withColor(color).withColor(color).withUnbreakable(true).item().build(),
            new ItemBuilder(Material.LEATHER_CHESTPLATE).buildMeta(LeatherArmorMetaBuilder.class).withColor(color).withUnbreakable(true).item().build(),
            new ItemBuilder(Material.LEATHER_LEGGINGS).buildMeta(LeatherArmorMetaBuilder.class).withColor(color).withUnbreakable(true).item().build(),
            new ItemBuilder(Material.LEATHER_BOOTS).buildMeta(LeatherArmorMetaBuilder.class).withColor(color).withUnbreakable(true).item().build()
        );
    }

    public static List<ItemStack> getArmor(HCPlayer hcPlayer) {
        if (hcPlayer.hasTeam()) {
            Color color = Colors.toColor(hcPlayer.getTeam().getColor());
            return Lists.newArrayList(
                new ItemBuilder(Material.LEATHER_HELMET).buildMeta(LeatherArmorMetaBuilder.class).withColor(color).withColor(color).withUnbreakable(true).item().build(),
                new ItemBuilder(Material.LEATHER_CHESTPLATE).buildMeta(LeatherArmorMetaBuilder.class).withColor(color).withUnbreakable(true).item().build(),
                new ItemBuilder(Material.LEATHER_LEGGINGS).buildMeta(LeatherArmorMetaBuilder.class).withColor(color).withUnbreakable(true).item().build(),
                new ItemBuilder(Material.LEATHER_BOOTS).buildMeta(LeatherArmorMetaBuilder.class).withColor(color).withUnbreakable(true).item().build()
            );
        }

        return new ArrayList<>();
    }

    public static List<ItemStack> getItems(RagePlayer ragePlayer) {
        return Lists.newArrayList(
            new ItemBuilder(Material.IRON_SWORD)
                .buildMeta()
                .withEnchant(Enchantment.DAMAGE_ALL, 255, true)
                .withUnbreakable(true)
                .withItemFlags(ItemFlag.HIDE_ENCHANTS)
                .item()
                .build(),
            new ItemBuilder(Material.BOW)
                .buildMeta()
                .withEnchant(Enchantment.ARROW_DAMAGE, 255, true)
                .withUnbreakable(true)
                .withItemFlags(ItemFlag.HIDE_ENCHANTS)
                .item()
                .build(),
            new ItemStack(Material.ARROW, ragePlayer.getArrowsPerSpawn())
        );
    }

    //////////////
    // Hardcoins
    /////////////

    public static boolean isHardcoinItem(ItemStack item) {
        if (item != null
            && item.getType().equals(Material.GOLD_NUGGET)
            && item.hasItemMeta()
            && item.getItemMeta().hasLore()) {
            return item.getItemMeta().getLore().containsAll(api.getStringsAPI().getList("rage.items.hardcoin.lore").toStringList());
        }
        return false;
    }

    public static List<ItemStack> getHardcoins(int amount) {
        List<ItemStack> hardcoins = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            hardcoins.add(getHardcoin());
        }
        return hardcoins;
    }

    public static ItemStack getHardcoin() {
        return new ItemBuilder(Material.GOLD_NUGGET)
            .buildMeta()
            .withDisplayName(api.getStringsAPI().get("rage.items.hardcoin.name").toString() + System.nanoTime())
            .withLore(
                api.getStringsAPI().getList("rage.items.hardcoin.lore")
                    .toStringList()
            )
            .withEnchant(Enchantment.ARROW_DAMAGE, 1, true)
            .withItemFlags(ItemFlag.HIDE_ENCHANTS)
            .item()
            .build();
    }


    ////////////
    // All tier
    ////////////

    public static ItemStack getItemWithAmount(ItemStack item, int amount) {
        item.setAmount(amount);
        return item;
    }


    //////////////
    // Tier 1
    //////////////

    public static ItemStack getTier1Gadget(RagePlayer ragePlayer) {
        int random3 = Randoms.randomInteger(1, 2);

        switch (random3) {
            default:
            case 1:
                return getTier1_1Gadget(ragePlayer);
            case 2:
                return getTier1_2Gadget(ragePlayer);
            case 3:
                return getTier1_3Gadget(ragePlayer);
        }

    }

    public static boolean isTier1_1Gadget(ItemStack item) {
        if (item != null
            && item.getType().equals(Gadget1_1Checker.itemMaterial)
            && item.hasItemMeta()
            && item.getItemMeta().getDisplayName() != null) {
            return item.getItemMeta().getDisplayName().equals(api.getStringsAPI().get("rage.items.tier-1-1.name").toString());
        }
        return false;
    }

    public static ItemStack getTier1_1Gadget(RagePlayer ragePlayer, int amount) {
        return getItemWithAmount(getTier1_1Gadget(ragePlayer), amount);
    }

    public static ItemStack getTier1_1Gadget(RagePlayer ragePlayer) {
        return new ItemBuilder(Gadget1_1Checker.itemMaterial)
            .buildMeta()
            .withDisplayName(api.getStringsAPI().get("rage.items.tier-1-1.name").toString())
            .withLore(
                api.getStringsAPI().getList("rage.items.tier-1-1.lore")
                    .replace("%value%", String.valueOf(ragePlayer.getTier1_1Value()))
                    .toStringList()
            )
            .withEnchant(Enchantment.ARROW_DAMAGE, 1, true)
            .withItemFlags(ItemFlag.HIDE_ENCHANTS)
            .item()
            .build();
    }

    public static ItemStack getTier1_2Gadget(RagePlayer ragePlayer, int amount) {
        return getItemWithAmount(getTier1_2Gadget(ragePlayer), amount);
    }

    public static boolean isTier1_2Gadget(ItemStack item) {
        if (item != null
            && item.getType().equals(Gadget1_2Checker.itemMaterial)
            && item.hasItemMeta()
            && item.getItemMeta().getDisplayName() != null) {
            return item.getItemMeta().getDisplayName().equals(api.getStringsAPI().get("rage.items.tier-1-2.name").toString());
        }
        return false;
    }

    public static ItemStack getTier1_2Gadget(RagePlayer ragePlayer) {
        return new ItemBuilder(Gadget1_2Checker.itemMaterial)
            .buildMeta()
            .withDisplayName(api.getStringsAPI().get("rage.items.tier-1-2.name").toString())
            .withLore(
                api.getStringsAPI().getList("rage.items.tier-1-2.lore")
                    .replace("%value%", String.valueOf(ragePlayer.getTier1_2Value()))
                    .toStringList()
            )
            .withEnchant(Enchantment.ARROW_DAMAGE, 1, true)
            .withItemFlags(ItemFlag.HIDE_ENCHANTS)
            .item()
            .build();
    }

    public static ItemStack getTier1_3Gadget(RagePlayer ragePlayer, int amount) {
        return getItemWithAmount(getTier1_3Gadget(ragePlayer), amount);
    }

    public static boolean isTier1_3Gadget(ItemStack item) {
        if (item != null
            && item.getType().equals(Gadget1_3Checker.itemMaterial)
            && item.hasItemMeta()
            && item.getItemMeta().getDisplayName() != null) {
            return item.getItemMeta().getDisplayName().equals(api.getStringsAPI().get("rage.items.tier-1-3.name").toString());
        }
        return false;
    }

    public static ItemStack getTier1_3Gadget(RagePlayer ragePlayer) {
        return new ItemBuilder(Gadget1_3Checker.itemMaterial)
            .buildMeta()
            .withDisplayName(api.getStringsAPI().get("rage.items.tier-1-3.name").toString())
            .withLore(
                api.getStringsAPI().getList("rage.items.tier-1-3.lore")
                    .replace("%value%", String.valueOf(ragePlayer.getTier1_3Value()))
                    .toStringList()
            )
            .withEnchant(Enchantment.ARROW_DAMAGE, 1, true)
            .withItemFlags(ItemFlag.HIDE_ENCHANTS)
            .item()
            .build();
    }


    //////////////
    // Tier 2
    //////////////

    public static ItemStack getTier2Gadget(RagePlayer ragePlayer) {
        int random3 = Randoms.randomInteger(1, 3);

        switch (random3) {
            default:
            case 1:
                return getTier2_1Gadget(ragePlayer);
            case 2:
                return getTier2_2Gadget(ragePlayer);
            case 3:
                return getTier2_3Gadget(ragePlayer);
        }

    }

    public static boolean isTier2_1Gadget(ItemStack item) {
        if (item != null
            && item.getType().equals(Gadget2_1Checker.itemMaterial)
            && item.hasItemMeta()
            && item.getItemMeta().getDisplayName() != null) {
            return item.getItemMeta().getDisplayName().equals(api.getStringsAPI().get("rage.items.tier-2-1.name").toString());
        }
        return false;
    }


    public static ItemStack getTier2_1Gadget(RagePlayer ragePlayer, int amount) {
        return getItemWithAmount(getTier2_1Gadget(ragePlayer), amount);
    }

    public static ItemStack getTier2_1Gadget(RagePlayer ragePlayer) {
        return new ItemBuilder(Gadget2_1Checker.itemMaterial)
            .buildMeta()
            .withDisplayName(api.getStringsAPI().get("rage.items.tier-2-1.name").toString())
            .withLore(
                api.getStringsAPI().getList("rage.items.tier-2-1.lore")
                    .replace("%value%", String.valueOf(ragePlayer.getTier2_1Value()))
                    .toStringList()
            )
            .withEnchant(Enchantment.ARROW_DAMAGE, 1, true)
            .withItemFlags(ItemFlag.HIDE_ENCHANTS)
            .item()
            .build();
    }

    public static boolean isTier2_2Gadget(ItemStack item) {
        if (item != null
            && item.getType().equals(Gadget2_2Checker.itemMaterial)
            && item.hasItemMeta()
            && item.getItemMeta().getDisplayName() != null) {
            return item.getItemMeta().getDisplayName().equals(api.getStringsAPI().get("rage.items.tier-2-2.name").toString());
        }
        return false;
    }


    public static ItemStack getTier2_2Gadget(RagePlayer ragePlayer, int amount) {
        return getItemWithAmount(getTier2_2Gadget(ragePlayer), amount);
    }

    public static ItemStack getTier2_2Gadget(RagePlayer ragePlayer) {
        return new ItemBuilder(Gadget2_2Checker.itemMaterial)
            .buildMeta()
            .withDisplayName(api.getStringsAPI().get("rage.items.tier-2-2.name").toString())
            .withLore(
                api.getStringsAPI().getList("rage.items.tier-2-2.lore")
                    .replace("%value%", String.valueOf(ragePlayer.getTier2_2Value()))
                    .toStringList()
            )
            .withEnchant(Enchantment.ARROW_DAMAGE, 1, true)
            .withItemFlags(ItemFlag.HIDE_ENCHANTS)
            .item()
            .build();
    }

    public static boolean isTier2_3Gadget(ItemStack item) {
        if (item != null
            && item.getType().equals(Gadget2_3Checker.itemMaterial)
            && item.hasItemMeta()
            && item.getItemMeta().getDisplayName() != null) {
            return item.getItemMeta().getDisplayName().equals(api.getStringsAPI().get("rage.items.tier-2-3.name").toString());
        }
        return false;
    }


    public static ItemStack getTier2_3Gadget(RagePlayer ragePlayer, int amount) {
        return getItemWithAmount(getTier2_3Gadget(ragePlayer), amount);
    }

    public static ItemStack getTier2_3Gadget(RagePlayer ragePlayer) {
        return new ItemBuilder(Gadget2_3Checker.itemMaterial)
            .buildMeta()
            .withDisplayName(api.getStringsAPI().get("rage.items.tier-2-3.name").toString())
            .withLore(
                api.getStringsAPI().getList("rage.items.tier-2-3.lore")
                    .replace("%value%", String.valueOf(ragePlayer.getTier2_3Value()))
                    .toStringList()
            )
            .withEnchant(Enchantment.ARROW_DAMAGE, 1, true)
            .withItemFlags(ItemFlag.HIDE_ENCHANTS)
            .item()
            .build();
    }


    //////////////
    // Tier 3
    //////////////

    public static ItemStack getTier3Gadget(RagePlayer ragePlayer) {
        int random3 = Randoms.randomInteger(1, 3);

        switch (random3) {
            default:
            case 1:
                return getTier3_1Gadget(ragePlayer);
            case 2:
                return getTier3_2Gadget(ragePlayer);
            case 3:
                return getTier3_3Gadget(ragePlayer);
        }

    }

    public static boolean isTier3_1Gadget(ItemStack item) {
        if (item != null
            && item.getType().equals(Gadget3_1Checker.itemMaterial)
            && item.hasItemMeta()
            && item.getItemMeta().getDisplayName() != null) {
            return item.getItemMeta().getDisplayName().equals(api.getStringsAPI().get("rage.items.tier-3-1.name").toString());
        }
        return false;
    }

    public static ItemStack getTier3_1Gadget(RagePlayer ragePlayer, int amount) {
        return getItemWithAmount(getTier3_1Gadget(ragePlayer), amount);
    }

    public static ItemStack getTier3_1Gadget(RagePlayer ragePlayer) {
        return new ItemBuilder(Gadget3_1Checker.itemMaterial)
            .buildMeta()
            .withDisplayName(api.getStringsAPI().get("rage.items.tier-3-1.name").toString())
            .withLore(
                api.getStringsAPI().getList("rage.items.tier-3-1.lore")
                    .replace("%value%", String.valueOf(ragePlayer.getTier3_1Value()))
                    .toStringList()
            )
            .withEnchant(Enchantment.ARROW_DAMAGE, 1, true)
            .withItemFlags(ItemFlag.HIDE_ENCHANTS)
            .item()
            .build();
    }

    public static boolean isTier3_2Gadget(ItemStack item) {
        if (item != null
            && item.getType().equals(Gadget3_2Checker.itemMaterial)
            && item.hasItemMeta()
            && item.getItemMeta().getDisplayName() != null) {
            return item.getItemMeta().getDisplayName().equals(api.getStringsAPI().get("rage.items.tier-3-2.name").toString());
        }
        return false;
    }


    public static ItemStack getTier3_2Gadget(RagePlayer ragePlayer, int amount) {
        return getItemWithAmount(getTier3_2Gadget(ragePlayer), amount);
    }

    public static ItemStack getTier3_2Gadget(RagePlayer ragePlayer) {
        return new ItemBuilder(Gadget3_2Checker.itemMaterial)
            .buildMeta()
            .withDisplayName(api.getStringsAPI().get("rage.items.tier-3-2.name").toString())
            .withLore(
                api.getStringsAPI().getList("rage.items.tier-3-2.lore")
                    .replace("%value%", String.valueOf(ragePlayer.getTier3_2Value()))
                    .toStringList()
            )
            .withEnchant(Enchantment.ARROW_DAMAGE, 1, true)
            .withItemFlags(ItemFlag.HIDE_ENCHANTS)
            .item()
            .build();
    }

    public static boolean isTier3_3Gadget(ItemStack item) {
        if (item != null
            && item.getType().equals(Gadget3_3Checker.itemMaterial)
            && item.hasItemMeta()
            && item.getItemMeta().getDisplayName() != null) {
            return item.getItemMeta().getDisplayName().equals(api.getStringsAPI().get("rage.items.tier-3-3.name").toString());
        }
        return false;
    }


    public static ItemStack getTier3_3Gadget(RagePlayer ragePlayer, int amount) {
        return getItemWithAmount(getTier3_3Gadget(ragePlayer), amount);
    }

    public static ItemStack getTier3_3Gadget(RagePlayer ragePlayer) {
        return new ItemBuilder(Gadget3_3Checker.itemMaterial)
            .buildMeta()
            .withDisplayName(api.getStringsAPI().get("rage.items.tier-3-3.name").toString())
            .withLore(
                api.getStringsAPI().getList("rage.items.tier-3-3.lore")
                    .replace("%value%", String.valueOf(ragePlayer.getTier3_3Value()))
                    .toStringList()
            )
            .withEnchant(Enchantment.ARROW_DAMAGE, 1, true)
            .withItemFlags(ItemFlag.HIDE_ENCHANTS)
            .item()
            .build();
    }

}
