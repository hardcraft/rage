package com.gmail.val59000mc.rage.listeners;

import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.events.HCAfterPlayEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerKilledByPlayerEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerStartEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.rage.common.Constants;
import com.gmail.val59000mc.rage.events.DoubleJumpEvent;
import com.gmail.val59000mc.rage.players.RagePlayer;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

public class XpListener extends HCListener {

    // jump xp
    private int xpLevelsPerJump;
    private int xpLevelsPerKill;
    private int xpLevelsRegainBaseMilliseconds;
    private int xpLevelsMax;

    @EventHandler
    public void afterLoad(HCAfterLoadEvent e) {
        FileConfiguration cfg = getApi().getConfig();

        this.xpLevelsPerJump = cfg.getInt("double-jump.xp-levels-per-jump", Constants.DEFAULT_XP_LEVELS_PER_JUMP);
        this.xpLevelsPerKill = cfg.getInt("double-jump.xp-levels-per-kills", Constants.DEFAULT_XP_LEVELS_PER_KILL);
        this.xpLevelsRegainBaseMilliseconds = cfg.getInt("double-jump.xp-levels-regain-base-milliseconds", Constants.DEFAULT_XP_LEVELS_REGAIN_BASE_SECONDS);
        this.xpLevelsMax = cfg.getInt("double-jump.xp-levels-max", Constants.DEFAULT_XP_LEVELS_MAX);
    }

    /**
     * Check xp bar when a double jump occur
     *
     * @param e
     */
    @EventHandler(priority = EventPriority.HIGH)
    public void onDoubleJumpAttempt(DoubleJumpEvent e) {
        if (e.isUnlimitedJump()) {
            // don't check xp bar
        } else {
            RagePlayer ragePlayer = e.getRagePlayer();
            Player player = ragePlayer.getPlayer();
            int playerLevel = player.getLevel();
            if (playerLevel >= xpLevelsPerJump) {
                player.setLevel(playerLevel - xpLevelsPerJump);
            } else {
                getStringsApi().get("rage.double-jump.not-enough-levels")
                    .replace("%minLevels%", String.valueOf(xpLevelsPerJump))
                    .sendChat(ragePlayer);
                getSoundApi().play(ragePlayer, Sound.ENTITY_VILLAGER_NO, 0.5f, 2);
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onKill(HCPlayerKilledByPlayerEvent e) {
        Player killer = e.getHcKiller().getPlayer();
        int level = addXp(killer.getLevel(), xpLevelsPerKill);
        killer.setLevel(level);
    }

    @EventHandler
    public void onStart(HCPlayerStartEvent e) {
        RagePlayer ragePlayer = (RagePlayer) e.getHcPlayer();
        ragePlayer.updateLastXpRegain();

        // give initial level when player starts game
        if (ragePlayer.isOnline()) {
            Player killer = e.getHcPlayer().getPlayer();
            int level = addXp(killer.getLevel(), xpLevelsPerKill);
            killer.setLevel(level);
        }
    }

    @EventHandler
    public void onPlay(HCAfterPlayEvent e) {
        getApi().buildTask("xp regain task", (task) -> {

            long now = System.currentTimeMillis();
            for (HCPlayer hcPlayer : getPmApi().getPlayers(true, PlayerState.PLAYING)) {
                RagePlayer ragePlayer = (RagePlayer) hcPlayer;
                long lastRegain = ragePlayer.getLastXpRegain();
                boolean shouldGainXp = (now - lastRegain) * ragePlayer.getXpRegainSpeed() >= xpLevelsRegainBaseMilliseconds;
                if (shouldGainXp) {
                    Player player = ragePlayer.getPlayer();
                    player.setLevel(addXp(player.getLevel(), 1));
                    if (player.getLevel() != 30) {
                        getSoundApi().play(ragePlayer, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 0.05f, 2);
                    }
                    ragePlayer.updateLastXpRegain();
                }
            }
        })
            .withInterval(20)
            .build()
            .start();
    }


    /**
     * increase xp value capped by max xp
     *
     * @return
     */
    private int addXp(int value, int increment) {
        int sum = value + increment;
        if (sum >= xpLevelsMax)
            return xpLevelsMax;
        else
            return sum;
    }

}
