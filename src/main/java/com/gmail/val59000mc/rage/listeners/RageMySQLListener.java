package com.gmail.val59000mc.rage.listeners;

import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBInsertedEvent;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBPersistedSessionEvent;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.rage.players.RagePlayer;
import com.gmail.val59000mc.spigotutils.Logger;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import javax.sql.rowset.CachedRowSet;
import java.sql.SQLException;

public class RageMySQLListener extends HCListener {

    // Queries
    private String createRagePlayerSQL;
    private String createRagePlayerStatsSQL;
    private String createRagePlayerPerksSQL;
    private String insertRagePlayerSQL;
    private String insertRagePlayerStatsSQL;
    private String insertRagePlayerPerksSQL;
    private String updateRagePlayerGlobalStatsSQL;
    private String updateRagePlayerStatsSQL;
    private String selectRagePlayerPerksSQL;

    /**
     * Load sql queries from resources files
     */
    public void readQueries() {
        HCMySQLAPI sql = getMySQLAPI();
        if (sql.isEnabled()) {
            createRagePlayerSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/create_rage_player.sql");
            createRagePlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/create_rage_player_stats.sql");
            createRagePlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/create_rage_player_perks.sql");
            insertRagePlayerSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/insert_rage_player.sql");
            insertRagePlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/insert_rage_player_stats.sql");
            insertRagePlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/insert_rage_player_perks.sql");
            updateRagePlayerGlobalStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/update_rage_player_global_stats.sql");
            updateRagePlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/update_rage_player_stats.sql");
            selectRagePlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/select_rage_player_perks.sql");
        }
    }

    /**
     * Insert game if not exists
     *
     * @param e
     */
    @EventHandler
    public void onGameFinishedLoading(HCAfterLoadEvent e) {
        HCMySQLAPI sql = getMySQLAPI();

        readQueries();

        if (sql.isEnabled()) {

            Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
                @Override
                public void run() {

                    try {

                        sql.execute(sql.prepareStatement(createRagePlayerSQL));

                        sql.execute(sql.prepareStatement(createRagePlayerStatsSQL));

                        sql.execute(sql.prepareStatement(createRagePlayerPerksSQL));

                    } catch (SQLException e) {
                        Logger.severe("Couldnt create tables for Rage stats or perks");
                        e.printStackTrace();
                    }

                }
            });
        }
    }

    /**
     * Add new player if not exists when joining
     * Update name (because it may change)
     * Update current played game
     *
     * @param e
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(HCPlayerDBInsertedEvent e) {
        HCMySQLAPI sql = getMySQLAPI();

        if (sql.isEnabled()) {

            Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
                @Override
                public void run() {

                    try {
                        String id = String.valueOf(e.getHcPlayer().getId());

                        // Insert rage player if not exists
                        sql.execute(sql.prepareStatement(insertRagePlayerSQL, id));

                        // Insert rage player stats if not exists
                        sql.execute(sql.prepareStatement(insertRagePlayerStatsSQL, id, sql.getMonth(), sql.getYear()));

                        sql.execute(sql.prepareStatement(insertRagePlayerPerksSQL, id));

                        CachedRowSet perks = sql.query(sql.prepareStatement(selectRagePlayerPerksSQL, id));
                        perks.first();
                        RagePlayer ragePlayer = (RagePlayer) e.getHcPlayer();
                        ragePlayer.setArrowsPerSpawn(perks.getInt("arrows_per_spawn"));
                        ragePlayer.setXpRegainSpeed(perks.getInt("xp_regain_speed"));
                        ragePlayer.setGadgetsLevel(perks.getInt("gagdets_level"));

                    } catch (SQLException ex) {
                        Log.severe("Couldn't find perks for player " + e.getHcPlayer().getName());
                        ex.printStackTrace();
                    }
                }
            });
        }
    }


    /**
     * Save all players global data when game ends
     *
     * @param e
     */
    @EventHandler
    public void onGameEnds(HCPlayerDBPersistedSessionEvent e) {
        HCMySQLAPI sql = getMySQLAPI();

        if (sql.isEnabled()) {

            RagePlayer ragePlayer = (RagePlayer) e.getHcPlayer();

            // Launch one async task to save all data
            Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
                @Override
                public void run() {

                    try {
                        // Update rage player global stats
                        sql.execute(sql.prepareStatement(updateRagePlayerGlobalStatsSQL,
                            String.valueOf(ragePlayer.getBestKillStreak()),
                            String.valueOf(ragePlayer.getTotalTier1Gadgets()),
                            String.valueOf(ragePlayer.getTotalTier2Gadgets()),
                            String.valueOf(ragePlayer.getTotalTier3Gadgets()),
                            String.valueOf(ragePlayer.getDoubleJumps()),
                            String.valueOf(ragePlayer.getTimePlayed()),
                            String.valueOf(ragePlayer.getKills()),
                            String.valueOf(ragePlayer.getDeaths()),
                            String.valueOf(ragePlayer.getMoney()),
                            String.valueOf(ragePlayer.getWins()),
                            String.valueOf(ragePlayer.getId())
                        ));


                        // Update rage player stats
                        sql.execute(sql.prepareStatement(updateRagePlayerStatsSQL,
                            String.valueOf(ragePlayer.getBestKillStreak()),
                            String.valueOf(ragePlayer.getTotalTier1Gadgets()),
                            String.valueOf(ragePlayer.getTotalTier2Gadgets()),
                            String.valueOf(ragePlayer.getTotalTier3Gadgets()),
                            String.valueOf(ragePlayer.getDoubleJumps()),
                            String.valueOf(ragePlayer.getTimePlayed()),
                            String.valueOf(ragePlayer.getKills()),
                            String.valueOf(ragePlayer.getDeaths()),
                            String.valueOf(ragePlayer.getMoney()),
                            String.valueOf(ragePlayer.getWins()),
                            String.valueOf(ragePlayer.getId()),
                            sql.getMonth(),
                            sql.getYear()
                        ));

                    } catch (SQLException e) {
                        Logger.severe("Couldnt update Rage player stats for player=" + ragePlayer.getName() + " uuid=" + ragePlayer.getUuid());
                        e.printStackTrace();
                    }

                }
            });
        }

    }
}
