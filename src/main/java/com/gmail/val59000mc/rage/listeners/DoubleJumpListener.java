package com.gmail.val59000mc.rage.listeners;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.rage.common.Constants;
import com.gmail.val59000mc.rage.events.DoubleJumpEvent;
import com.gmail.val59000mc.rage.players.RagePlayer;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;

public class DoubleJumpListener extends HCListener {

    // jump velocity
    private double horizontalVelocity;
    private double verticalVelocity;

    public DoubleJumpListener(HCGameAPI api) {
        setApi(api);
        FileConfiguration cfg = getApi().getConfig();

        this.horizontalVelocity = cfg.getDouble("double-jump.horizontal-velocity", Constants.DEFAULT_DOUBLE_JUMP_HORIZONTAL_VELOCITY);
        this.verticalVelocity = cfg.getDouble("double-jump.vertical-velocity", Constants.DEFAULT_DOUBLE_JUMP_VERTICAL_VELOCITY);
    }

    /**
     * Check if player atttempts to double jump while in air
     *
     * @param e
     */
    @EventHandler
    public void onToggleFlight(PlayerToggleFlightEvent e) {
        if (GameMode.SURVIVAL.equals(e.getPlayer().getGameMode())) {
            Player player = e.getPlayer();

            HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
            if (hcPlayer != null && hcPlayer.isPlaying()) {
                DoubleJumpEvent jumpEvent = getApi().callEvent(new DoubleJumpEvent(getApi(), (RagePlayer) hcPlayer));
                if (!jumpEvent.isCancelled()) {
                    player.setVelocity(
                        player.getLocation()
                            .getDirection()
                            .normalize()
                            .multiply(horizontalVelocity)
                            .setY(verticalVelocity)
                    );
                }
            }

            e.setCancelled(true);
            player.setAllowFlight(false);

            getApi().sync(() -> {
                player.setAllowFlight(true);
            }, 5);
        }
    }

    /**
     * Check if player is on ground then allow flight
     *
     * @param e
     */
    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerMove(PlayerMoveEvent e) {
        if (!e.getPlayer().getAllowFlight()) {
            Player player = e.getPlayer();
            Block underFeet = player.getLocation()
                .subtract(0, 1, 0)
                .getBlock();
            if (underFeet.getType().isSolid())
                player.setAllowFlight(true);
        }
    }


    /**
     * What do do when a double jump can be performed and is not cancelled
     *
     * @param e
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onActualDoubleJump(DoubleJumpEvent e) {
        RagePlayer ragePlayer = e.getRagePlayer();
        ragePlayer.addDoubleJumps();
        Player player = ragePlayer.getPlayer();
        Location location = player.getLocation();
        getSoundApi().play(Sound.ENTITY_FIREWORK_ROCKET_LAUNCH, location, 0.5f, 2);
        location.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, location, 10, 1, 1, 1, 1);
    }

}
