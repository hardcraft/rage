package com.gmail.val59000mc.rage.listeners;

import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.rage.specialzones.SpecialZoneManager;
import org.bukkit.event.EventHandler;

public class SpecialZonesListener extends HCListener {

    private SpecialZoneManager manager;

    @EventHandler
    public void afterLoad(HCAfterLoadEvent e) {
        manager = new SpecialZoneManager(getApi());
    }

}
