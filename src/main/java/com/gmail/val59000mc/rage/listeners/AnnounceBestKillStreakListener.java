package com.gmail.val59000mc.rage.listeners;

import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;
import com.gmail.val59000mc.hcgameslib.events.HCBeforeEndAllPlayerEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.rage.common.Constants;
import com.gmail.val59000mc.rage.players.RagePlayer;
import com.gmail.val59000mc.spigotutils.Numbers;
import org.bukkit.event.EventHandler;

public class AnnounceBestKillStreakListener extends HCListener {

    @EventHandler
    public void beforeEndAllPlayers(HCBeforeEndAllPlayerEvent e) {

        RagePlayer bestKillStreakPlayer = getBestKillStreakPlayer();
        if (bestKillStreakPlayer != null) {

            HCMessage msg = getStringsApi().get("rage.best-kill-streak-player")
                .replace("%player%", bestKillStreakPlayer.getColoredName())
                .replace("%killStreak%", String.valueOf(bestKillStreakPlayer.getBestKillStreak()));

            getApi().sync(() -> {
                double rewarded = getPmApi().rewardMoneyTo(
                    bestKillStreakPlayer,
                    getConfig().getDouble("kill-streak-reward", Constants.DEFAULT_KILL_STREAK_REWARD)
                );
                msg.replace("%money%", String.valueOf(Numbers.round(rewarded, 2)));
                msg.sendChatP();
            }, 20);
        }


    }

    private RagePlayer getBestKillStreakPlayer() {
        RagePlayer best = null;
        for (HCPlayer hcPlayer : getPmApi().getPlayers(true, PlayerState.PLAYING)) {
            RagePlayer ragePlayer = (RagePlayer) hcPlayer;
            if (ragePlayer.getBestKillStreak() > 0 && (best == null || ragePlayer.getBestKillStreak() > best.getBestKillStreak())) {
                best = ragePlayer;
            }
        }
        return best;
    }
}
