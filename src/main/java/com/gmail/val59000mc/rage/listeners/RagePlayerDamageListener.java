package com.gmail.val59000mc.rage.listeners;

import com.gmail.val59000mc.hcgameslib.events.HCPlayerDamagedByHCPlayerEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDamagedEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.potion.PotionEffectType;

public class RagePlayerDamageListener extends HCListener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDamage(HCPlayerDamagedEvent event) {
        EntityDamageEvent e = event.getWrapped();
        if (!e.isCancelled()
            && (
            e.getCause().equals(DamageCause.FALL)
                || ((Player) e.getEntity()).hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE)
        )
        ) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onCrossKillDamage(HCPlayerDamagedByHCPlayerEvent event) {
        EntityDamageEvent e = event.getWrapped();
        if (!e.isCancelled()
            && event.getHcDamager().getPlayer().isDead()
        ) {
            e.setCancelled(true);
        }
    }


    @EventHandler(priority = EventPriority.HIGH)
    public void onExplosionDamage(HCPlayerDamagedEvent event) {
        EntityDamageEvent e = event.getWrapped();
        if (!e.isCancelled()
            && (
            e.getCause().equals(DamageCause.ENTITY_EXPLOSION)
        )
        ) {
            e.setDamage(10000);
        }
    }

}
