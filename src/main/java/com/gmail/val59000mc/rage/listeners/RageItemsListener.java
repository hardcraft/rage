package com.gmail.val59000mc.rage.listeners;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.rage.items.RageItems;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class RageItemsListener extends HCListener {

    @EventHandler
    public void onItemPickup(final PlayerPickupItemEvent event) {
        // Deny picking up arrows
        if (event.getItem().getItemStack().getType().equals(Material.ARROW)) {
            event.setCancelled(true);
        } else if (RageItems.isHardcoinItem(event.getItem().getItemStack())) {
            event.getItem().remove();
            event.setCancelled(true);

            HCPlayer hcPlayer = getPmApi().getHCPlayer(event.getPlayer());
            if (hcPlayer != null && hcPlayer.isPlaying()) {
                getSoundApi().play(hcPlayer, Sound.ENTITY_CHICKEN_EGG, 1f, 2f);
                getPmApi().rewardMoneyNoBonusTo(hcPlayer, 1);
                hcPlayer.getScoreboard().update();
            }
        }
    }

    // deny throwing all items
    @EventHandler
    public void onItemPickup(PlayerDropItemEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onProjectileHit(ProjectileHitEvent e) {
        if (e.getEntity() instanceof Arrow) {
            Entity arrow = e.getEntity();
            getApi().sync(() -> {
                if (arrow.isValid())
                    arrow.remove();
            }, 40);
        }
    }

}
