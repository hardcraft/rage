package com.gmail.val59000mc.rage.listeners;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

public class RageBlockListener extends HCListener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPlace(BlockPlaceEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEntityExplosion(EntityExplodeEvent e) {
        e.blockList().clear();
    }

    @EventHandler
    public void onBlockExplosion(BlockExplodeEvent e) {
        e.blockList().clear();
    }
}
