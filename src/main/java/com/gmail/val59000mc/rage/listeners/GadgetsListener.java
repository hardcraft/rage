package com.gmail.val59000mc.rage.listeners;

import com.gmail.val59000mc.hcgameslib.events.HCPlayerDeathEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerQuitEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerRespawnEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.rage.events.ActivateGadgetEvent;
import com.gmail.val59000mc.rage.gadgets.checker.*;
import com.gmail.val59000mc.rage.players.RagePlayer;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.ItemCompareOption;
import com.google.common.collect.Sets;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.*;

public class GadgetsListener extends HCListener {

    private static Set<Action> interactActions = Sets.newHashSet(
        Action.RIGHT_CLICK_AIR,
        Action.RIGHT_CLICK_BLOCK
    );
    private Map<RagePlayer, List<ItemStack>> savedGagdetsOnDeath;
    private Map<Material, IGadgetChecker> gadgetsCheckers;


    public GadgetsListener() {
        this.savedGagdetsOnDeath = new HashMap<>();

        this.gadgetsCheckers = new HashMap<>();
        registerGadgetCheck(
            new Gadget1_1Checker(),
            new Gadget1_2Checker(),
            new Gadget1_3Checker(),
            new Gadget2_1Checker(),
            new Gadget2_2Checker(),
            new Gadget2_3Checker(),
            new Gadget3_1Checker(),
            new Gadget3_2Checker(),
            new Gadget3_3Checker()
        );

    }

    private void registerGadgetCheck(IGadgetChecker... gadgetCheckers) {
        for (IGadgetChecker gadgetChecker : gadgetCheckers) {
            gadgetsCheckers.put(gadgetChecker.getMaterial(), gadgetChecker);
        }
    }

    @EventHandler
    public void onUseItem(PlayerInteractEvent e) {
        if (interactActions.contains(e.getAction())) {
            ItemStack item = e.getItem();

            // fast checking if item is obviously not a gadget trigger
            if (item == null || !gadgetsCheckers.containsKey(item.getType()))
                return;

            RagePlayer ragePlayer = (RagePlayer) getPmApi().getHCPlayer(e.getPlayer());
            if (ragePlayer.isPlaying()) {

                // getting gadget checker matching item material
                IGadgetChecker gadgetChecker = gadgetsCheckers.get(item.getType());

                if (gadgetChecker.isGadgetItem(item)) {

                    ActivateGadgetEvent activateGadgetEvent = gadgetChecker.getActivationEvent(getApi(), ragePlayer);

                    getApi().callEvent(activateGadgetEvent);

                    // remove 1 item if event gadget was run
                    if (!activateGadgetEvent.isCancelled()) {
                        e.setCancelled(true);
                        ItemStack toRemove = item.clone();
                        toRemove.setAmount(1);
                        Inventories.remove(e.getPlayer().getInventory(), toRemove, new ItemCompareOption.Builder()
                            .withDisplayName(true)
                            .withEnchantments(true)
                            .withMaterial(true)
                            .build()
                        );
                    }

                }

            }

        }
    }

    // activate gadget if activation event is not cancelled
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onActivateGadget(ActivateGadgetEvent e) {
        e.getGadget().activate(getApi());
    }

    // handle saveing gadgets on death
    @EventHandler
    public void onDeath(HCPlayerDeathEvent e) {

        RagePlayer ragePlayer = (RagePlayer) e.getHcPlayer();
        if (!ragePlayer.isOnline())
            return;

        ItemStack[] inv = ragePlayer.getPlayer().getInventory().getContents();

        List<ItemStack> savedGadgets = new ArrayList<>();

        for (IGadgetChecker gadgetChecker : gadgetsCheckers.values()) {
            gadgetChecker.addSavedGagets(ragePlayer, inv, savedGadgets);
        }

        if (savedGadgets.size() > 0) {
            savedGagdetsOnDeath.put(ragePlayer, savedGadgets);
        }
    }

    @EventHandler
    public void onRespawn(HCPlayerRespawnEvent e) {
        RagePlayer ragePlayer = (RagePlayer) e.getHcPlayer();
        List<ItemStack> savedGadgets = savedGagdetsOnDeath.get(ragePlayer);
        if (savedGadgets != null && ragePlayer.isOnline()) {
            PlayerInventory inv = ragePlayer.getPlayer().getInventory();

            for (ItemStack savedGagdet : savedGadgets) {
                inv.addItem(savedGagdet);
            }
        }

        // clear saved gagdets wheter we have given it back or not (if player was offline too bad for him)
        savedGagdetsOnDeath.remove(ragePlayer);
    }

    @EventHandler
    public void onDisconnect(HCPlayerQuitEvent e) {
        savedGagdetsOnDeath.remove(e.getHcPlayer());
    }

}
