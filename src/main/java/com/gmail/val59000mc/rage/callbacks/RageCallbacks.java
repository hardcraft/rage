package com.gmail.val59000mc.rage.callbacks;

import com.gmail.val59000mc.hcgameslib.api.impl.DefaultPluginCallbacks;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.stuff.Stuff;
import com.gmail.val59000mc.rage.common.Constants;
import com.gmail.val59000mc.rage.items.RageItems;
import com.gmail.val59000mc.rage.listeners.DoubleJumpListener;
import com.gmail.val59000mc.rage.players.RagePlayer;
import com.gmail.val59000mc.rage.players.RageTeam;
import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.spigotutils.Texts;
import com.gmail.val59000mc.spigotutils.Time;
import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;

public class RageCallbacks extends DefaultPluginCallbacks {

    private boolean enableNightVision;
    private int killsLimit;

    @Override
    public void beforeLoad() {
        enableNightVision = getConfig().getBoolean("night-vision", Constants.DEFAULT_ENABLE_NIGHT_VISION);
        killsLimit = getConfig().getInt("kills-limit", Constants.DEFAULT_KILL_LIMIT);
    }

    @Override
    public HCTeam newHCTeam(String name, ChatColor color, Location spawnpoint) {
        return new RageTeam(name, color, spawnpoint);
    }

    @Override
    public HCPlayer newHCPlayer(Player player) {
        return new RagePlayer(player);
    }

    @Override
    public HCPlayer newHCPlayer(HCPlayer hcPlayer) {
        return new RagePlayer(hcPlayer);
    }

    @Override
    public List<Stuff> createStuffs() {
        RageItems.setup(getApi());
        return super.createStuffs();
    }

    @Override
    public List<HCTeam> createTeams() {
        List<HCTeam> teams = new ArrayList<HCTeam>();

        World world = getApi().getWorldConfig().getWorld();

        FileConfiguration cfg = getConfig();

        RageTeam redTeam = (RageTeam) newHCTeam(Constants.RED_TEAM, ChatColor.RED, Parser.parseLocation(world, cfg.getString("teams.red")));

        RageTeam blueTeam = (RageTeam) newHCTeam(Constants.BLUE_TEAM, ChatColor.BLUE, Parser.parseLocation(world, getConfig().getString("teams.blue")));

        RageTeam orangeTeam = (RageTeam) newHCTeam(Constants.ORANGE_TEAM, ChatColor.GOLD, Parser.parseLocation(world, cfg.getString("teams.orange")));

        RageTeam greenTeam = (RageTeam) newHCTeam(Constants.GREEN_TEAM, ChatColor.GREEN, Parser.parseLocation(world, cfg.getString("teams.green")));


        if (getConfig().getInt("teams.number", Constants.DEFAULT_TEAMS_NUMBER) == 4) {
            // 4 teams
            teams.add(redTeam);
            teams.add(blueTeam);
            teams.add(orangeTeam);
            teams.add(greenTeam);

        } else {
            // 2 teams
            teams.add(redTeam);
            teams.add(blueTeam);
        }

        return teams;
    }

    @Override
    public void assignStuffToPlayer(HCPlayer hcPlayer) {

        if (hcPlayer.getStuff() == null && hcPlayer.hasTeam() && hcPlayer.isOnline()) {
            hcPlayer.setStuff(
                new Stuff.Builder(hcPlayer.getName())
                    .addArmorItems(RageItems.getArmor(hcPlayer))
                    .addInventoryItems(RageItems.getItems((RagePlayer) hcPlayer))
                    .build()
            );
        }
    }


    /**
     * Update the scoreboard of a playing HCPlayer
     * Must return the lines of the scoreboard
     *
     * @return lines
     */
    @Override
    public List<String> updatePlayingScoreboard(HCPlayer hcPlayer) {

        List<String> content = new ArrayList<String>();

        // All teams kills
        content.add(getStringsApi().get("messages.scoreboard.scores").toString());
        for (HCTeam team : getPmApi().getTeams()) {
            content.add(" " + team.getColor() + team.getName() + " " + ChatColor.WHITE + team.getKills());
        }

        // Remaining time (if exists)
        if (getApi().is(GameState.PLAYING) && getApi().isCountdownEndOfGameEnabled()) {
            int remainingTime = getApi().getRemainingTimeBeforeEnd();
            content.add(
                getStringsApi().get("messages.scoreboard.remaining-time-before-end").toString() +
                    " " +
                    ChatColor.GREEN + Time.getFormattedTime(remainingTime)
            );
        }

        // Kills
        content.add(getStringsApi().get("messages.scoreboard.kills-deaths").toString());
        content.add(" " + ChatColor.GREEN + "" + hcPlayer.getKills() + ChatColor.WHITE + "/" + ChatColor.GREEN + hcPlayer.getDeaths());

        // Coins
        content.add(getStringsApi().get("messages.scoreboard.coins").toString());
        content.add(" " + ChatColor.GREEN + "" + hcPlayer.getMoney());

        RagePlayer ragePlayer = (RagePlayer) hcPlayer;

        // Kill Streak
        content.add(getStringsApi().get("rage.scoreboard.kill-streak.title").toString());
        content.add(" " + getStringsApi().get("rage.scoreboard.kill-streak.current").toString() + " " + ChatColor.GREEN + ragePlayer.getCurrentKillStreak());
        content.add(" " + getStringsApi().get("rage.scoreboard.kill-streak.best").toString() + " " + ChatColor.GREEN + ragePlayer.getBestKillStreak());


        return content;
    }

    @Override
    public void formatDeathMessage(HCPlayer hcKilled, PlayerDeathEvent event) {
        RagePlayer rageKilled = (RagePlayer) hcKilled;
        if (rageKilled.getCurrentKillStreak() >= 5) {
            event.setDeathMessage(getStringsApi()
                .get("rage.kill-streak.stopped")
                .replace("%killed%", rageKilled.getColoredName())
                .replace("%kills%", String.valueOf(rageKilled.getCurrentKillStreak()))
                .toString()
            );
            getSoundApi().play(Sound.ENTITY_WITHER_HURT, 0.8f, 2);
        } else {
            super.formatDeathMessage(hcKilled, event);
        }
    }

    @Override
    public void formatDeathMessage(HCPlayer hcKilled, HCPlayer hcKiller, PlayerDeathEvent event) {

        RagePlayer rageKilled = (RagePlayer) hcKilled;
        RagePlayer rageKiller = (RagePlayer) hcKiller;

        if (rageKilled.getCurrentKillStreak() >= 5) {
            event.setDeathMessage(getStringsApi()
                .get("rage.kill-streak.stopped-by")
                .replace("%killed%", rageKilled.getColoredName())
                .replace("%killer%", rageKiller.getColoredName())
                .replace("%kills%", String.valueOf(rageKilled.getCurrentKillStreak()))
                .toString()
            );
            getSoundApi().play(Sound.ENTITY_WITHER_HURT, 0.8f, 2);
        } else {
            super.formatDeathMessage(hcKilled, hcKiller, event);
        }

    }


    @Override
    public void handleDeathEvent(PlayerDeathEvent event, HCPlayer hcKilled) {

        super.handleDeathEvent(event, hcKilled);

        handleDeath(event, (RagePlayer) hcKilled);

    }

    @Override
    public void handleKillEvent(PlayerDeathEvent event, HCPlayer hcKilled, HCPlayer hcKiller) {

        super.handleKillEvent(event, hcKilled, hcKiller);

        RagePlayer rageKilled = (RagePlayer) hcKilled;
        RagePlayer rageKiller = (RagePlayer) hcKiller;

        int currentKillStreak = rageKiller.addCurrentKillStreak();


        if (currentKillStreak % 5 == 0) {
            getStringsApi()
                .get("rage.kill-streak.is-on-spree")
                .replace("%killer%", rageKiller.getColoredName())
                .replace("%kills%", String.valueOf(currentKillStreak))
                .sendChatP();
            getSoundApi().play(Sound.BLOCK_ANVIL_USE, 2, 2);
        }

        // give gadgets rewards on tiers levels

        if (rageKiller.isOnline()) {
            ItemStack item = null;

            // give 1 arrow if the kill was with an arrow
            HCPlayer hcArrowShooter = getPmApi().getLastDamagingPlayerArrowShooter(hcKilled);
            if (hcArrowShooter != null && hcArrowShooter.equals(hcKiller)) {
                rageKiller.getPlayer().getInventory().addItem(new ItemStack(Material.ARROW));
            }

            switch (currentKillStreak) {
                case 3:
                    item = RageItems.getTier1Gadget(rageKiller);
                    rageKiller.addTotalTier1Gadgets();
                    getStringsApi().get("rage.gadgets.tier-1.received").sendActionBar(rageKiller);
                    break;
                case 5:
                    item = RageItems.getTier2Gadget(rageKiller);
                    rageKiller.addTotalTier2Gadgets();
                    getStringsApi().get("rage.gadgets.tier-2.received").sendActionBar(rageKiller);
                    break;
                case 7:
                    item = RageItems.getTier3Gadget(rageKiller);
                    rageKiller.addTotalTier3Gadgets();
                    getStringsApi().get("rage.gadgets.tier-3.received").sendActionBar(rageKiller);
                    break;
            }

            if (item != null) {
                getSoundApi().play(rageKiller, Sound.BLOCK_NOTE_BLOCK_HARP, 1, 2);
                rageKiller.getPlayer().getInventory().addItem(item);
            }

        }

        handleDeath(event, rageKilled);

        checkIfTeamHasWon(hcKiller.getTeam());

    }

    private void handleDeath(PlayerDeathEvent event, RagePlayer rageKilled) {

        event.setDroppedExp(0);
        event.setKeepLevel(true);
        event.getDrops().clear();
        rageKilled.resetCurrentKillStreak();

    }

    private void checkIfTeamHasWon(HCTeam team) {
        if (team.getKills() >= killsLimit) {
            getApi().endGame(team);
        }
    }

    @Override
    public void waitPlayerAtLobby(HCPlayer hcPlayer) {
        super.waitPlayerAtLobby(hcPlayer);
        if (hcPlayer.isOnline()) {
            addPlayerSpawningEffects(hcPlayer.getPlayer());
        }
    }

    @Override
    public void spectatePlayer(HCPlayer hcPlayer) {
        super.spectatePlayer(hcPlayer);
        if (hcPlayer.isOnline()) {
            addPlayerSpawningEffects(hcPlayer.getPlayer());
        }
    }

    @Override
    public void respawnPlayer(HCPlayer hcPlayer) {

        if (hcPlayer.isOnline()) {
            hcPlayer.getPlayer().setSaturation(20);
            addPlayerSpawningEffects(hcPlayer.getPlayer());
            getApi().getItemsAPI().giveStuffItemsToPlayer(hcPlayer);
        }

    }

    @Override
    public void startPlayer(HCPlayer hcPlayer) {
        super.startPlayer(hcPlayer);
        if (hcPlayer.isOnline()) {
            Player player = hcPlayer.getPlayer();
            player.setLevel(0);
            player.setGameMode(GameMode.SURVIVAL);
            addPlayerSpawningEffects(hcPlayer.getPlayer());
        }
    }

    private void addPlayerSpawningEffects(Player player) {
        Effects.add(player, PotionEffectType.DAMAGE_RESISTANCE, 50, 200, false);
        Effects.addPermanent(player, PotionEffectType.SPEED, 4, false);
        if (enableNightVision) {
            Effects.addPermanent(player, PotionEffectType.NIGHT_VISION, 0, false);
        }
    }


    @Override
    public void playGameMessages() {

        super.playGameMessages();

        if (getConfig().getBoolean("double-jump.enable", Constants.DEFAULT_ENABLE_DOUBLE_JUMP)) {
            getApi().registerListener(new DoubleJumpListener(getApi()));
        }


    }

    @Override
    public void printEndMessage(HCPlayer hcPlayer, HCTeam winningTeam) {

        if (hcPlayer.isOnline()) {

            RagePlayer ragePlayer = (RagePlayer) hcPlayer;

            Texts.tellraw(hcPlayer.getPlayer(), getStringsApi().get("messages.end.winning-team")
                .replace("%game%", getApi().getName())
                .replace("%kills%", String.valueOf(hcPlayer.getKills()))
                .replace("%deaths%", String.valueOf(hcPlayer.getDeaths()))
                .replace("%hardcoins%", String.valueOf(hcPlayer.getMoney()))
                .replace("%doubleJumps%", String.valueOf(ragePlayer.getDoubleJumps()))
                .replace("%bestKillStreak%", String.valueOf(ragePlayer.getBestKillStreak()))
                .replace("%tier1%", String.valueOf(ragePlayer.getTotalTier1Gadgets()))
                .replace("%tier2%", String.valueOf(ragePlayer.getTotalTier2Gadgets()))
                .replace("%tier3%", String.valueOf(ragePlayer.getTotalTier3Gadgets()))
                .replace("%timeplayed%", String.valueOf(hcPlayer.getTimePlayed()))
                .replace("%winner%", (winningTeam == null ? "Match nul" : winningTeam.getColor() + winningTeam.getName()))
                .toString()
            );

        }

    }

}
