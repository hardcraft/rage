package com.gmail.val59000mc.rage.common;

public class Constants {

    public static final int DEFAULT_TEAMS_NUMBER = 2;
    public static final String RED_TEAM = "Rouge";
    public static final String BLUE_TEAM = "Bleu";
    public static final String ORANGE_TEAM = "Orange";
    public static final String GREEN_TEAM = "Vert";

    public static final boolean DEFAULT_ENABLE_NIGHT_VISION = true;

    public static final int DEFAULT_KILL_LIMIT = 30;

    public static final boolean DEFAULT_ENABLE_DOUBLE_JUMP = true;
    public static final int DEFAULT_XP_LEVELS_PER_JUMP = 4;
    public static final int DEFAULT_XP_LEVELS_PER_KILL = 2;
    public static final int DEFAULT_XP_LEVELS_MAX = 30;
    public static final int DEFAULT_XP_LEVELS_REGAIN_BASE_SECONDS = 150000;
    public static final double DEFAULT_DOUBLE_JUMP_HORIZONTAL_VELOCITY = 1.6d;
    public static final double DEFAULT_DOUBLE_JUMP_VERTICAL_VELOCITY = 1.0d;

    public static final int DEFAULT_JUMP_ZONE_MULTIPLIER = 15;
    public static final double DEFAULT_KILL_STREAK_REWARD = 10;


}
