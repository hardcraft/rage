package com.gmail.val59000mc.rage.players;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import org.bukkit.entity.Player;

public class RagePlayer extends HCPlayer {

    private long lastXpRegain;

    // stats
    private int currentKillStreak;
    private int bestKillStreak;

    private int totalTier1Gadgets;
    private int totalTier2Gadgets;
    private int totalTier3Gadgets;
    private int doubleJumps;

    // perks
    private int arrowsPerSpawn;
    private int xpRegainSpeed;
    private int gadgetsLevel;


    public RagePlayer(Player player) {
        super(player);

        this.lastXpRegain = 0;

        this.currentKillStreak = 0;
        this.bestKillStreak = 0;
        this.totalTier1Gadgets = 0;
        this.totalTier2Gadgets = 0;
        this.totalTier3Gadgets = 0;
        this.doubleJumps = 0;

        this.arrowsPerSpawn = 2;
        this.xpRegainSpeed = 1;
        this.gadgetsLevel = 1;
    }

    public RagePlayer(HCPlayer hcPlayer) {
        super(hcPlayer);
        RagePlayer ragePlayer = (RagePlayer) hcPlayer;

        this.currentKillStreak = ragePlayer.currentKillStreak;
        this.bestKillStreak = ragePlayer.bestKillStreak;
        this.totalTier1Gadgets = ragePlayer.totalTier1Gadgets;
        this.totalTier2Gadgets = ragePlayer.totalTier2Gadgets;
        this.totalTier3Gadgets = ragePlayer.totalTier3Gadgets;
        this.doubleJumps = ragePlayer.doubleJumps;

        this.arrowsPerSpawn = ragePlayer.arrowsPerSpawn;
        this.xpRegainSpeed = ragePlayer.xpRegainSpeed;
        this.gadgetsLevel = ragePlayer.gadgetsLevel;
    }

    @Override
    public void subtractBy(HCPlayer lastUpdatedSession) {
        super.subtractBy(lastUpdatedSession);
        RagePlayer ragePlayer = (RagePlayer) lastUpdatedSession;
        this.totalTier1Gadgets -= ragePlayer.totalTier1Gadgets;
        this.totalTier2Gadgets -= ragePlayer.totalTier2Gadgets;
        this.totalTier3Gadgets -= ragePlayer.totalTier3Gadgets;
        this.doubleJumps -= ragePlayer.doubleJumps;
    }


    public long getLastXpRegain() {
        return lastXpRegain;
    }

    public void updateLastXpRegain() {
        this.lastXpRegain = System.currentTimeMillis();
    }

    public int getCurrentKillStreak() {
        return currentKillStreak;
    }

    public int addCurrentKillStreak() {
        this.currentKillStreak++;
        if (currentKillStreak > bestKillStreak) {
            bestKillStreak = currentKillStreak;
        }
        return currentKillStreak;
    }

    public void resetCurrentKillStreak() {
        this.currentKillStreak = 0;
    }

    public int getBestKillStreak() {
        return bestKillStreak;
    }


    // tier 1 gadget

    public int getTotalTier1Gadgets() {
        return totalTier1Gadgets;
    }

    public void addTotalTier1Gadgets() {
        this.totalTier1Gadgets++;
    }

    /**
     * Number of arrows
     *
     * @return
     */
    public int getTier1_1Value() {
        return 2 * gadgetsLevel + 1;
    }

    /**
     * Propulsion strenght
     *
     * @return
     */
    public int getTier1_2Value() {
        return 2 * gadgetsLevel;
    }

    /**
     * ?
     *
     * @return
     */
    public int getTier1_3Value() {
        return gadgetsLevel;
    }


    // tier 2 gadget

    public int getTotalTier2Gadgets() {
        return totalTier2Gadgets;
    }

    public void addTotalTier2Gadgets() {
        this.totalTier2Gadgets++;
    }

    /**
     * Radius of explosion
     *
     * @return
     */
    public int getTier2_1Value() {
        return 2 * gadgetsLevel + 1;
    }

    /**
     * Flame thrower duration
     *
     * @return
     */
    public int getTier2_2Value() {
        return 4 * gadgetsLevel;
    }

    /**
     * ?
     *
     * @return
     */
    public int getTier2_3Value() {
        return 8 * gadgetsLevel;
    }


    // tier 3 gadgets

    public int getTotalTier3Gadgets() {
        return totalTier3Gadgets;
    }

    public void addTotalTier3Gadgets() {
        this.totalTier3Gadgets++;
    }

    /**
     * Duration of jump
     *
     * @return
     */
    public int getTier3_1Value() {
        return gadgetsLevel * 10;
    }

    /**
     * Invincibility duration
     *
     * @return
     */
    public int getTier3_2Value() {
        return gadgetsLevel * 6;
    }

    /**
     * Duration of double coins
     *
     * @return
     */
    public int getTier3_3Value() {
        return 10 * gadgetsLevel;
    }


    // perks

    public int getDoubleJumps() {
        return doubleJumps;
    }

    public void addDoubleJumps() {
        this.doubleJumps++;
    }

    public int getXpRegainSpeed() {
        return xpRegainSpeed;
    }

    public void setXpRegainSpeed(int jumpRegainSpeed) {
        this.xpRegainSpeed = jumpRegainSpeed;
    }

    public int getArrowsPerSpawn() {
        return arrowsPerSpawn;
    }

    public void setArrowsPerSpawn(int arrowsPerSpawn) {
        this.arrowsPerSpawn = arrowsPerSpawn;
    }

    public int getGadgetsLevel() {
        return gadgetsLevel;
    }

    public void setGadgetsLevel(int gadgetLevel) {
        this.gadgetsLevel = gadgetLevel;
    }


}
