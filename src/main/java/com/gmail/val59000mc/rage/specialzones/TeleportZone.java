package com.gmail.val59000mc.rage.specialzones;

import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Sounds;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class TeleportZone {

    private LocationBounds bounds;
    private Location teleportTo;


    public TeleportZone(LocationBounds bounds, Location teleportTo) {
        super();
        this.bounds = bounds;
        this.teleportTo = teleportTo;
    }

    public boolean contains(Player player) {
        return bounds.contains(player.getLocation());
    }

    public void teleport(Player player) {
        player.teleport(teleportTo);
        Sounds.play(player, Sound.ENTITY_ENDERMAN_TELEPORT, 1, 2);
    }
}	
