package com.gmail.val59000mc.rage.specialzones;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.rage.common.Constants;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Parser;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class SpecialZoneManager {

    private HCGameAPI api;

    private List<JumpZone> jumps;
    private List<TeleportZone> teleports;

    public SpecialZoneManager(HCGameAPI api) {
        this.api = api;
        this.jumps = new ArrayList<>();
        this.teleports = new ArrayList<>();
        load();
    }

    private void load() {

        World world = api.getWorldConfig().getWorld();

        ConfigurationSection jumpsSection = api.getConfig().getConfigurationSection("special-zones.jump");
        if (jumpsSection != null) {
            for (String key : jumpsSection.getKeys(false)) {
                ConfigurationSection section = jumpsSection.getConfigurationSection(key);

                String minStr = section.getString("min");
                String maxStr = section.getString("max");
                int multiplier = section.getInt("multiplier", Constants.DEFAULT_JUMP_ZONE_MULTIPLIER);

                jumps.add(
                    new JumpZone(
                        new LocationBounds(
                            Parser.parseLocation(world, minStr),
                            Parser.parseLocation(world, maxStr)
                        ),
                        multiplier
                    )
                );
            }
        }

        api.buildTask("jump task", (task) -> {
            for (HCPlayer hcPlayer : api.getPlayersManagerAPI().getPlayers(true, PlayerState.PLAYING)) {
                Player player = hcPlayer.getPlayer();
                for (JumpZone jumpZone : jumps) {
                    if (jumpZone.contains(player)) {
                        jumpZone.applyEffect(player);
                    }
                }
            }
        })
            .withInterval(5)
            .build()
            .start();

        api.buildTask("teleport task", (task) -> {
            for (HCPlayer hcPlayer : api.getPlayersManagerAPI().getPlayers(true, PlayerState.PLAYING)) {
                Player player = hcPlayer.getPlayer();
                for (TeleportZone jumpZone : teleports) {
                    if (jumpZone.contains(player)) {
                        jumpZone.teleport(player);
                    }
                }
            }
        })
            .withInterval(10)
            .build()
            .start();

    }
}
