package com.gmail.val59000mc.rage.specialzones;

import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

public class JumpZone {
    private final static int EFFECT_DURATION = 20; // ticks

    private LocationBounds bounds;
    private int effectMultiplier;

    public JumpZone(LocationBounds bounds, int effectMultiplier) {
        super();
        this.bounds = bounds;
        this.effectMultiplier = effectMultiplier;
    }

    public boolean contains(Player player) {
        return bounds.contains(player.getLocation());
    }

    public void applyEffect(Player player) {
        Effects.add(player, PotionEffectType.JUMP, EFFECT_DURATION, effectMultiplier, false);
    }
}	
