package com.gmail.val59000mc.rage;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.rage.callbacks.RageCallbacks;
import com.gmail.val59000mc.rage.commands.RageTestCommand;
import com.gmail.val59000mc.rage.listeners.*;
import com.gmail.val59000mc.spigotutils.Configurations;
import com.google.common.collect.Sets;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Rage extends JavaPlugin {


    public void onEnable() {

        this.getDataFolder().mkdirs();
        File config = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "config.yml"), new File(this.getDataFolder(), "config.yml"));
        File lang = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "lang.yml"), new File(this.getDataFolder(), "lang.yml"));


        HCGameAPI game = new HCGame.Builder("Rage", this, config, lang)
            .withPluginCallbacks(new RageCallbacks())
            .withDefaultListenersAnd(Sets.newHashSet(
                new RageItemsListener(),
                new RageBlockListener(),
                new RagePlayerDamageListener(),
                new GadgetsListener(),
                new XpListener(),
                new SpecialZonesListener(),
                new RageMySQLListener(),
                new AnnounceBestKillStreakListener()
            ))
            .withDefaultCommandsAnd(Sets.newHashSet(
                new RageTestCommand(this)
            ))
            .build();
        game.loadGame();


    }

    public void onDisable() {
    }
}
