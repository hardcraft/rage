UPDATE rage_player_stats
SET best_kill_streak=GREATEST(best_kill_streak,?),
    tier_1_gadgets=tier_1_gadgets+?,
    tier_2_gadgets=tier_2_gadgets+?,
    tier_3_gadgets=tier_3_gadgets+?,
    double_jumps=double_jumps+?,
    play_time=play_time+?,
    kills=kills+?,
    deaths=deaths+?,
    coins_earned=coins_earned+?,
    wins=wins+?
WHERE player_id=?
  AND MONTH=?
  AND YEAR=?;