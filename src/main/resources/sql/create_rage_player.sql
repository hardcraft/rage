CREATE TABLE IF NOT EXISTS `rage_player` (
  `player_id` INT(11) NOT NULL,
  `best_kill_streak` INT(11) NOT NULL DEFAULT 0,
  `tier_1_gadgets` INT(11) NOT NULL DEFAULT 0,
  `tier_2_gadgets` INT(11) NOT NULL DEFAULT 0,
  `tier_3_gadgets` INT(11) NOT NULL DEFAULT 0,
  `double_jumps` INT(11) NOT NULL DEFAULT 0,
  `play_time` INT(11) NOT NULL DEFAULT 0,
  `kills` INT(11) NOT NULL DEFAULT 0,
  `deaths` INT(11) NOT NULL DEFAULT 0,
  `coins_earned` DECIMAL(19,4) NOT NULL DEFAULT 0,
  `wins` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`player_id`),
  UNIQUE INDEX `player_id_UNIQUE` (`player_id` ASC),
  CONSTRAINT `fk_rage_player_id`
    FOREIGN KEY (`player_id`)
    REFERENCES `player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;