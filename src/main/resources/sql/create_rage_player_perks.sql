CREATE TABLE IF NOT EXISTS `rage_player_perks` (
  `player_id` INT(11) NOT NULL,
  `arrows_per_spawn` INT NOT NULL DEFAULT 2,
  `xp_regain_speed` INT NOT NULL DEFAULT 1,
  `gagdets_level` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`player_id`),
  UNIQUE INDEX `player_id_UNIQUE` (`player_id` ASC),
  CONSTRAINT `fk_rage_player_perks_id`
    FOREIGN KEY (`player_id`)
    REFERENCES `player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;